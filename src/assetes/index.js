import Chain from "./chain.svg";
import Back from "./back.svg";
import Logo from "./logo.svg";
import Rules from "./rules.svg";
import Algorithm from "./algorithm.svg";
import Cloud from "./cloud.svg";
import Add from "./add.svg";
import Calendar from "./calendar.svg";
import Door from "./door.svg";
import User from "./user.svg";
import Minus from "./minus.svg";

export {
  Logo,
  Add,
  Door,
  User,
  Back,
  Rules,
  Minus,
  Chain,
  Cloud,
  Algorithm,
  Calendar
};
