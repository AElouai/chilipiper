import React from "react";

const SideBar = () => (
  <nav className="col-md-2 d-none d-md-block bg-light sidebar">
    <div className="sidebar-sticky">
      <ul className="nav flex-column">
        <li className="nav-item">
          <a className="nav-link active" href="#">
            Marketing
          </a>
        </li>
        <li className="nav-item">
          <a className="nav-link" href="#">
            Route Marketing Tooles
          </a>
        </li>
      </ul>
    </div>
  </nav>
);

export default SideBar;
