import React from "react";
import Back from "../../../assetes/back.svg";
import Card from "../../cards";

const Dashboard = () => (
  <div className="row">
    <h4>
      <img src={Back} /> Edit Booking Link
    </h4>
    <div class={"col-12"}>
      <Card />
    </div>
  </div>
);

export default Dashboard;
