import React from "react";
import Logo from "../../assetes/logo.svg";
import "./index.css";

const Header = () => (
  <nav className="navbar fixed-top flex-md-nowrap p-0 shadow">
    <a className="navbar-brand col-sm-3 col-md-2 mr-0" href="#">
      <img src={Logo} className={"img-responsive "} />
    </a>

    <a className="pr-5" href="#">
      login
    </a>
  </nav>
);

export default Header;
