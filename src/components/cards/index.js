import React, { Fragment } from "react";
import {
  Chain,
  Algorithm,
  Door,
  Cloud,
  Add,
  Calendar,
  User
} from "../../assetes";
import "./index.css";
import Rules from "./Rules";

const Params = [
  {
    title: "Rules",
    body:
      "Filter which events get assigned.For instance only events with accounts in San Francisco",
    img: Chain,
    actions: <Rules />
  },
  {
    title: "Algorithm",
    body: "Round Robin/Assign to owner/Free select",
    img: Algorithm,
    actions: <Rules />
  },
  {
    title: "Online Booking URL",
    body: "Booking hours",
    img: Cloud,
    actions: <Rules />
  },
  {
    title: "Meeting Settings",
    body: "Meeting template and availability",
    img: Calendar,
    actions: <Rules />
  },
  {
    title: "Advanced Meeting Settings",
    body: "Meting ownership & Salesforce settings",
    img: Calendar,
    actions: <Rules />
  },
  {
    title: "Active Meeting Rooms",
    body: "Optional, for in person meetings",
    img: Door,
    actions: <Rules />
  },
  {
    title: "Active Users",
    body: "Optional, for in person meetings",
    img: User,
    actions: <Rules />
  }
];

const SideBar = () => (
  <div className={"card-bg p-4"}>
    <div className={"row mb-3"}>
      <img className={"svg-width pt-4"} src={Chain} />

      <div className="col-5">
        <label htmlFor="name" className={"name-code"}>
          Name
        </label>
        <input
          type="text"
          className="form-control"
          id="name"
          placeholder="Inbound Demos holder"
        />
      </div>
    </div>
    {Params.map(item => (
      <div className={"row box"}>
        <img className={"svg-width"} src={item.img} />

        <div className="col-11 pt-2">
          <b className={""}>{item.title} </b>
          <p className={"name-code"}>{item.body}</p>
        </div>
        {item.actions}
      </div>
    ))}
  </div>
);

export default SideBar;
