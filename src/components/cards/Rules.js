import React, { PureComponent, Fragment } from "react";
import { Add, Minus } from "../../assetes";

export class Rules extends PureComponent {
  state = {
    show: false
  };

  showModal = () => this.setState({ show: true });
  hideModal = () => this.setState({ show: false });

  render() {
    const { show } = this.state;
    return (
      <Fragment>
        {!show && <img src={Add} onClick={this.showModal} />}
        {show && <img src={Minus} onClick={this.hideModal} />}
        {show && (
          <div className={"col-12"}>
            <div className="jumbotron">
              <h1 className="display-4">Hello, Rules!</h1>
              <p className="lead">this is where should go other rules</p>
            </div>
          </div>
        )}
      </Fragment>
    );
  }
}

export default Rules;
