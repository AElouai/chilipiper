import React, { Component, Fragment } from "react";
import Header from "../components/header";
import Sidebar from "../components/sidebar";
import { Dashboard } from "../components/container";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import "./index.css";

class Layout extends Component {
  render() {
    return (
      <BrowserRouter>
        <Fragment>
          <Header />
          <div className="container-fluid">
            <div className="row pt-5">
              <Sidebar />
              <main role="main" className="col-md-9 ml-sm-auto col-lg-10 px-4">
                <Switch>
                  <Route exact path="/" component={Dashboard} />
                  <Route component={Dashboard} />
                </Switch>
              </main>
            </div>
          </div>
        </Fragment>
      </BrowserRouter>
    );
  }
}

export default Layout;
